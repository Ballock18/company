package com.company;

public class Cliente extends Usuario {

    String localizacao;

    Cliente(String nome, String email, int Id, String localizacao){
        super(nome,email,Id);
        this.localizacao = localizacao;
    }

    void exibirDados(){
        System.out.printf("Nome: "+nome);
        System.out.println("\nE-mail: "+email);
        System.out.println("ID: "+Id);
        System.out.println("Localização escolhida: "+localizacao);
    }



}
