package com.company;
import java.util.Scanner;

public class TestaProj {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        Scanner entradaString = new Scanner(System.in);

        //dados do Cliente
        String nome;
        String email;
        String localizacao;
        int id;

        System.out.println("Digite o seu nome: ");
        nome = entradaString.nextLine();
        System.out.println("Digite o seu e-mail: ");
        email = entradaString.nextLine();
        System.out.println("Digite o seu código de identificação: ");
        id = entrada.nextInt();
        System.out.println("Digite sua localização: ");
        localizacao = entradaString.nextLine();

        Cliente c1 = new Cliente(nome,email,id,localizacao);
        System.out.println("Cadastro realizado com sucesso!");
        c1.exibirDados();
        System.out.println("");

        // dados do Produto
        String nomeProd;
        String marca;

        System.out.println("Olá, "+nome+". Qual produto você gostaria de buscar?");
        nomeProd = entradaString.nextLine();
        System.out.println(nome+", qual é a marca do produto que você deseja?");
        marca = entradaString.nextLine();

        Produto p1 = new Produto(nomeProd,marca);

        System.out.println("");
        System.out.println("Verifiquei que encontrei o seu produto!!!");
        System.out.println("");
        p1.exibirDadosProduto();


    }
}
